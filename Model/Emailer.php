<?php
/**
 * @package Gpcrocker_ShipmentTracking
 * @author Graham Crocker <graham.paul.crocker@gmail.com>
 */
namespace Gpcrocker\ShipmentTracking\Model;

use Magento\Framework\App\Area;
use Magento\Framework\DataObject;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Payment\Helper\Data;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Address\Renderer;
use Magento\Sales\Model\Order\Shipment;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class Emailer
 * @package Gpcrocker\ShipmentTracking\Model
 */
class Emailer
{
    const EMAIL_TEMPLATE = 'sales_email_tracking_guest_template';

    /**
     * @var TransportBuilder
     */
    private $transportBuilder;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var Data
     */
    private $paymentHelper;

    /**
     * @var Renderer
     */
    protected $addressRenderer;

    /**
     * @param TransportBuilder $transportBuilder
     * @param StoreManagerInterface $storeManager
     * @param OrderRepositoryInterface $orderRepository
     * @param Data $paymentHelper
     * @param Renderer $addressRenderer
     */
    public function __construct(
        TransportBuilder $transportBuilder,
        StoreManagerInterface $storeManager,
        OrderRepositoryInterface $orderRepository,
        Data $paymentHelper,
        Renderer $addressRenderer
    ) {
        $this->transportBuilder = $transportBuilder;
        $this->storeManager = $storeManager;
        $this->orderRepository = $orderRepository;
        $this->paymentHelper = $paymentHelper;
        $this->addressRenderer = $addressRenderer;
    }

    /**
     * @param Shipment $shipment
     * @param bool $notify
     * @param string $comment
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\MailException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function send(Shipment $shipment, $notify = true, $comment = '')
    {
        $orderId = $shipment->getOrderId();
        $order = $this->orderRepository->get($orderId);

        $transport = [
            'order' => $order,
            'shipment' => $shipment,
            'comment' => $comment ? $comment->getComment() : '',
            'billing' => $order->getBillingAddress(),
            'payment_html' => $this->getPaymentHtml($order),
            'store' => $order->getStore(),
            'formattedShippingAddress' => $this->getFormattedShippingAddress($order),
            'formattedBillingAddress' => $this->getFormattedBillingAddress($order)
        ];

        $this->transportBuilder
            ->setTemplateIdentifier(self::EMAIL_TEMPLATE)
            ->setTemplateOptions(
                [
                    'area' => Area::AREA_FRONTEND,
                    'store' => $this->storeManager->getStore()->getId(),
                ]
            )
            ->setTemplateVars($transport)
            ->setFromByScope(
                "sales",
                $this->storeManager->getStore()->getId()
            )->addTo(
                $order->getCustomerEmail(),
                $order->getBillingAddress()->getName()
            );
        $transport = $this->transportBuilder->getTransport();
        $transport->sendMessage();
    }

    /**
     * Returns payment info block as HTML.
     *
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     *
     * @return string
     * @throws \Exception
     */
    private function getPaymentHtml(\Magento\Sales\Api\Data\OrderInterface $order)
    {
        return $this->paymentHelper->getInfoBlockHtml(
            $order->getPayment(),
            $this->storeManager->getStore()->getStoreId()
        );
    }

    /**
     * Render shipping address into html.
     *
     * @param Order $order
     * @return string|null
     */
    protected function getFormattedShippingAddress($order)
    {
        return $order->getIsVirtual()
            ? null
            : $this->addressRenderer->format($order->getShippingAddress(), 'html');
    }

    /**
     * Render billing address into html.
     *
     * @param Order $order
     * @return string|null
     */
    protected function getFormattedBillingAddress($order)
    {
        return $this->addressRenderer->format($order->getBillingAddress(), 'html');
    }
}
