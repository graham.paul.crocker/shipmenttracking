#  Gpcrocker_ShipmentTracking Module
Gpcrocker Shipment Tracking Email Module for Magento2

## Description

The module is responsible for sending an email while adding tracking information to a shipment.

## Installation

- Place the Module inside Magento2 `app/code` directory
- Run `bin/magento module:enable Gpcrocker_ShipmentTracking --clear-static-content`
- Run `php bin/magento setup:upgrade`
- In Production mode Run `bin/magento setup:static-content:deploy && bin/magento setup:di:compile`
- Finally run `php bin/magento clean:cache`

## Instructions

- Get Admin token

`curl --request POST \
   --url http://magento2.test/rest/V1/integration/admin/token \
   --header 'content-type: multipart/form-data; boundary=---011000010111000001101001' \
   --form username= \
   --form password=`
- Create Cart

`curl --request POST \
   --url http://magento2.test/rest/V1/guest-carts/ \
   --header 'authorization: Bearer <token>'`
- Get Cart Info

`curl --request GET \
   --url http://magento2.test/rest/V1/guest-carts/Qi0hvGb3zfYrsI2kSc1FWTbIoW9GfGJh \
   --header 'authorization: Bearer <token>'`
- Add Product to Cart

`curl --request POST \
   --url http://magento2.test/rest/V1/guest-carts/Qi0hvGb3zfYrsI2kSc1FWTbIoW9GfGJh/items \
   --header 'authorization: Bearer <token> ' \
   --header 'content-type: application/json' \
   --data '{
 	"cartItem" : {
 		"quote_id" : "Qi0hvGb3zfYrsI2kSc1FWTbIoW9GfGJh",
 		"sku" : "24-MB05",
 		"qty" : 1
 	}
 }'`
 
 - Add Shipping Information
 
 `curl --request POST \
    --url http://magento2.test/rest/V1/guest-carts/Qi0hvGb3zfYrsI2kSc1FWTbIoW9GfGJh/shipping-information \
    --header 'authorization: Bearer <token>' \
    --header 'content-type: application/json' \
    --data '{
      "addressInformation": {
          "shippingAddress": {
              "region": "MH",
              "region_id": 0,
              "country_id": "GB",
              "street": [
                  "x y street"
              ],
              "company": "Test",
              "telephone": "1111111",
              "postcode": "12223",
              "city": "Manchester",
              "firstname": "Graham",
              "lastname": "Crocker",
              "email": "gpcrocker@gmail.com",
              "prefix": "address_",
              "region_code": "AA",
              "sameAsBilling": 1
          },
          "billingAddress": {
              "region": "MH",
              "region_id": 0,
              "country_id": "GB",
              "street": [
                  "x,y street"
              ],
              "company": "abc",
              "telephone": "1111111",
              "postcode": "12223",
              "city": "Manchester",
              "firstname": "Graham",
              "lastname": "Crocker",
              "email": "gpcrocker@gmail.com",
              "prefix": "address_",
              "region_code": "AA"
          },
          "shipping_method_code": "flatrate",
          "shipping_carrier_code": "flatrate"
      }
  }'`

- Get Payment Method

`curl --request GET \
   --url http://magento2.test/rest/V1/guest-carts/Qi0hvGb3zfYrsI2kSc1FWTbIoW9GfGJh/payment-information \
   --header 'authorization: Bearer <token>'`
   
- Place Order

`curl --request PUT \
   --url http://magento2.test/rest/V1/guest-carts/Qi0hvGb3zfYrsI2kSc1FWTbIoW9GfGJh/order \
   --header 'authorization: Bearer <token>' \
   --header 'content-type: application/json' \
   --data '{
 	"paymentMethod" : {
 		"method" : "checkmo"
 	}
 }'`
 
 - Create Invoice
 
 `curl --request POST \
    --url http://magento2.test/rest/V1/order/3/invoice \
    --header 'authorization: Bearer <token>' \
    --header 'content-type: application/json' \
    --data '{
  	"capture" : true,
  	"notify" : true
  }'`
  
  
  - Create Shipping
  
  `curl --request POST \
     --url http://magento2.test/rest/V1/shipment/ \
     --header 'authorization: Bearer <token>' \
     --header 'content-type: application/json' \
     --data '{
     "entity":{
       "orderId":3,
      "items":[{"orderItemId":13,"qty":1}]
      }
   }'`
   
   - Add Tracking
   
   The module sends an email when tracking information is added to the shipping
   
  `curl --request POST \
     --url http://magento2.test/rest/V1/shipment/track \
     --header 'authorization: Bearer <token>' \
     --header 'content-type: application/json' \
     --data '{
     "entity": {
       "order_id": 3,
   		"parent_id" : 4,
       "qty": 1,
       "extension_attributes": {
   			"notify" : "1"
   		},
       "track_number": "12345678",
       "title": "ground",
       "carrier_code": "OOPS Delivery"
     }
   }'`
 
## Compatibility

Tested on Magento 2.3.5 and PHP 7.3

## License

Each source file included in this distribution is licensed under OSL 3.0

Open Software License (OSL 3.0). Please see LICENSE.txt for the full text of the OSL 3.0 license

## Contributor

- Graham Crocker
