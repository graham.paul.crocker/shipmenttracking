<?php
/**
 * @package Gpcrocker_ShipmentTracking
 * @author Graham Crocker <graham.paul.crocker@gmail.com>
 */
namespace Gpcrocker\ShipmentTracking\Plugin;

use Gpcrocker\ShipmentTracking\Model\Emailer;
use Magento\Sales\Api\Data\ShipmentTrackInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Api\ShipmentRepositoryInterface;
use Magento\Sales\Api\ShipmentTrackRepositoryInterface;

/**
 * Plugin to send an email after adding tracking information to a shipment
 *
 * Class SendShipmentTrackEmail
 * @package Gpcrocker\ShipmentTracking\Plugin
 */
class SendTrackingEmail
{
    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var ShipmentRepositoryInterface
     */
    protected $shipmentRepository;

    /**
     * @var Emailer
     */
    protected $emailer;

    /**
     * SendTrackingEmail constructor.
     * @param OrderRepositoryInterface $orderRepository
     * @param ShipmentRepositoryInterface $shipmentRepository
     * @param Emailer $emailer
     */
    public function __construct(
        OrderRepositoryInterface $orderRepository,
        ShipmentRepositoryInterface $shipmentRepository,
        Emailer $emailer
    ) {
        $this->orderRepository = $orderRepository;
        $this->shipmentRepository = $shipmentRepository;
        $this->emailer = $emailer;
    }

    /**
     * Send an email after adding tracking information to a shipment
     *
     * @param ShipmentTrackRepositoryInterface $shipmentRepository
     * @param ShipmentRepositoryInterface $entity
     * @return ShipmentRepositoryInterface
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @throws \Exception
     */
    public function afterSave(
        ShipmentTrackRepositoryInterface $shipmentRepository,
        ShipmentTrackInterface $entity
    ) {
        $extensionAttributes = $entity->getExtensionAttributes();
        if (!$extensionAttributes->getNotify()) {
            return $entity;
        }
        $parentId = $entity->getParentId();
        if ($shipment = $this->shipmentRepository->get($parentId)) {
            $this->emailer->send($shipment, true);
        }
        return $entity;
    }
}
