<?php
/**
 * @package Gpcrocker_ShipmentTracking
 * @author Graham Crocker <graham.paul.crocker@gmail.com>
 */
declare(strict_types=1);

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'Gpcrocker_ShipmentTracking',
    __DIR__
);
